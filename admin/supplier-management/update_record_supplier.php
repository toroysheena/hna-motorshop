<?php
include('../../connection.php');
session_start();

if(isset($_POST['updaterecord'])) {   
    $code = $_POST['edit_code'];
    $supplier_name = $_POST['edit_suppliername'];
    $supplier_address = $_POST['edit_address'];
    $contact_person = $_POST['edit_person'];
    $contact_number = $_POST['edit_number'];
    $email = $_POST['edit_email'];

    $query = "UPDATE supplier_record SET 
        supplier_name=?, supplier_address=?, contact_person=?, contact_number=?, email=?
        WHERE code=?";
    $stmt = mysqli_prepare($conn, $query);

    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "ssssss", $supplier_name, $supplier_address, $contact_person, $contact_number, $email, $code);
        $query_run = mysqli_stmt_execute($stmt);

        if($query_run) {
            $_SESSION['message'] = "Successfully Updated Account";
            $_SESSION['message_type'] = "success";            
            header("Location: ../supplier-management/records.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Update Account";
            $_SESSION['message_type'] = "danger";   
            header("Location: ../supplier-management/records.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Prepared statement error";
        $_SESSION['message_type'] = "danger";   
        header("Location: ../supplier-management/records.php");
        exit();
    }
}
?>
