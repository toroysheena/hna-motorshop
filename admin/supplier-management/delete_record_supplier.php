<?php 
include('../../connection.php');
session_start();

if(isset($_POST['recorddelete'])) {
    $code = $_POST['delete_code'];

    $query_delete = "DELETE FROM supplier_record WHERE code=?";
    $stmt_delete = mysqli_prepare($conn, $query_delete);

    if ($stmt_delete) {
        mysqli_stmt_bind_param($stmt_delete, "s", $code);
        $query_run_delete = mysqli_stmt_execute($stmt_delete);

        if($query_run_delete) {
            $_SESSION['message'] = "Successfully Deleted Account";
            $_SESSION['message_type'] = "success";
            header("Location: ../supplier-management/records.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Delete Account";
            $_SESSION['message_type'] = "danger";    
            header("Location: ../supplier-management/records.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Prepared statement error";
        $_SESSION['message_type'] = "danger";    
        header("Location: ../supplier-management/records.php");
        exit();
    }
}
?>
