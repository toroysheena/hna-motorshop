<?php
include('../../connection.php');
session_start();

if(isset($_POST['updaterecord'])) {   
    $code = $_POST['edit_code'];
    $services_type = $_POST['edit_service'];

    $query = "UPDATE services SET service=? WHERE service_id=?";
    $stmt = mysqli_prepare($conn, $query);

    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "ss", $services_type, $code);
        $query_run = mysqli_stmt_execute($stmt);

        if($query_run) {
            $_SESSION['message'] = "Successfully Updated Service";
            $_SESSION['message_type'] = "success";  
            header("Location: ../services-management/services.php");
            exit();
        } else {
            $_SESSION['message'] = "Failed to Update Account";
            $_SESSION['message_type'] = "danger";   
            header("Location: ../services-management/services.php");
            exit();
        }
    } else {
        $_SESSION['message'] = "Prepared statement error";
        $_SESSION['message_type'] = "danger";   
        header("Location: ../services-management/services.php");
        exit();
    }
}
?>

