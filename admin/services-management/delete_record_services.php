<?php 
include('../../connection.php');
session_start();

if(isset($_POST['recorddelete']))
{
    $code = $_POST['delete_code'];

    $query_delete_dependent = "DELETE FROM mechanic_services WHERE services_id=?";
    $stmt_delete_dependent = mysqli_prepare($conn, $query_delete_dependent);
    mysqli_stmt_bind_param($stmt_delete_dependent, "s", $code);
    $query_run_delete_dependent = mysqli_stmt_execute($stmt_delete_dependent);

    if ($query_run_delete_dependent) {
        $query_delete = "DELETE FROM services WHERE service_id=?";
        $stmt_delete = mysqli_prepare($conn, $query_delete);
        mysqli_stmt_bind_param($stmt_delete, "s", $code);
        $query_run_delete = mysqli_stmt_execute($stmt_delete);
        
        if($query_run_delete)
        {
            $_SESSION['message'] = "Successfully Deleted Service";
            $_SESSION['message_type'] = "success";
            header("Location: ../services-management/services.php");
            exit(); 
        }
        else
        {
            $_SESSION['message'] = "Failed to Delete Service";
            $_SESSION['message_type'] = "danger";    
            header("Location: ../services-management/services.php");
            exit(); 
        }
    } else {
        $_SESSION['message'] = "Unable to delete dependent records.";
        $_SESSION['message_type'] = "danger";    
        header("Location: ../services-management/services.php");
        exit(); 
    }
}
?>
