<?php
//main connection file for both admin & front end
$servername = "localhost"; //server
$username = "root"; //username
$password = ""; //password
$dbname = "herbnangel"; //database

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname); // connecting 
// Check connection
if(mysqli_connect_error())
{
    echo"<script>
        alert('Cannnot Connect to Database');
        window.location.href='index.php';
    </script>";
}
?>